package com.linguochao.design.structure.composite.organazition;

import lombok.Data;

@Data
public abstract class OrganizationComponent {

	private String name; // 名字
	private String des; // 说明

	protected  void add(OrganizationComponent organizationComponent) {
		//默认实现
		throw new UnsupportedOperationException();
	}

	protected  void remove(OrganizationComponent organizationComponent) {
		//默认实现
		throw new UnsupportedOperationException();
	}

	//构造器
	public OrganizationComponent(String name, String des) {
		super();
		this.name = name;
		this.des = des;
	}


	//方法print, 做成抽象的, 子类都需要实现
	protected abstract void print();


}