package com.linguochao.design.structure.bridge.brand;


public interface Brand {
	void open();
	void close();
	void call();
}
