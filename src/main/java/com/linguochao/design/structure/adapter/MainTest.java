package com.linguochao.design.structure.adapter;

import com.linguochao.design.structure.adapter.adapter.TurkeyAdapter;
import com.linguochao.design.structure.adapter.adapter.TurkeyAdapter2;
import com.linguochao.design.structure.adapter.duck.Duck;
import com.linguochao.design.structure.adapter.duck.GreenHeadDuck;
import com.linguochao.design.structure.adapter.turkey.WildTurkey;

public class MainTest {
	public static void main(String[] args) {

		GreenHeadDuck duck=new GreenHeadDuck();
		WildTurkey turkey=new WildTurkey();
		Duck duck1=new TurkeyAdapter2();
        Duck duck2 = new TurkeyAdapter(turkey);

        turkey.gobble();
		turkey.fly();

		duck.quack();
		duck.fly();

        duck1.quack();
        duck1.fly();

        duck2.quack();
        duck2.fly();
	
	}
}
