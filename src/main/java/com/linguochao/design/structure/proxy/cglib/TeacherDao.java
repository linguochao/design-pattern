package com.linguochao.design.structure.proxy.cglib;

public class TeacherDao {

	public String teach() {
		System.out.println("老师授课中... ");
		return "hello";
	}
}
