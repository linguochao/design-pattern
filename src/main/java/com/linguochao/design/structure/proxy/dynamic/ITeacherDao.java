package com.linguochao.design.structure.proxy.dynamic;


public interface ITeacherDao {

	void teach();

	void sayHello(String name);
}
