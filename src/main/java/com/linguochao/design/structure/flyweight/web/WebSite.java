package com.linguochao.design.structure.flyweight.web;

public abstract class WebSite {

	public abstract void use(User user);
}
