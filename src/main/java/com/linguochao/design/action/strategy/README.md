
#策略模式 -- 鸭子项目

### 需求说明:
- 有各种鸭子(比如野鸭、北京鸭、水鸭等，鸭子有各种行为，比如叫、飞行等)
- 显示鸭子的信


### 传统超类方法分析
- 其它鸭子，都继承了Duck类，所以fly让所有子类都会飞了，这是不正确的
- 上面说的1的问题，其实是继承带来的问题：对类的局部改动，尤其超类的局部改动，会影响其他部分。会有溢出效应
- 为了改进1问题，我们可以通过覆盖fly方法来解决=>覆盖解决
- 问题又来了，如果我们有一个玩具鸭子ToyDuck,这样就需要ToyDuck去覆盖Duck的所有实现的方法


### 策略模式(strategypattern)
定义了一系列算法，并将每个算法封装起来，使他们可以相互替换，且算法的变化不会影响到使用算法的客户。
需要设计一个接口，为一系列实现类提供统一的方法，多个实现类实现该接口，设计一个抽象类（可有可无，属于辅助类），提供辅助函数

### 策略模式的注意事项和细节
- 策略模式的关键是：分析项目中变化部分与不变部分
- 策略模式的核心思想是：多用组合/聚合少用继承；用行为类组合，而不是行为的继承。更有弹性
- 体现了“对修改关闭，对扩展开放”原则，客户端增加行为不用修改原有代码，只要添加一种策略（或者行为）即可，避免了使用多重转移语句（if..elseif..else）
- 提供了可以替换继承关系的办法：策略模式将算法封装在独立的Strategy类中使得你可以独立于其Context改变它，使它易于切换、易于理解、易于扩展
- 需要注意的是：每添加一个策略就要增加一个类，当策略过多是会导致类数目庞


**分析**
- 好处：新增行为简单，行为类更好的复用，组合更方便。既有继承带来的复用好处，没有挖坑
- 策略模式：分别封装行为接口，实现算法族，超类里放行为接口对象，在子类里具体设定行为对象。
- 原则就是：分离变化部分，封装接口，基于接口编程各种功能。此模式让行为算法的变化独立于算法的使用者。





