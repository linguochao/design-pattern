package com.linguochao.design.action.iterator;

import com.linguochao.design.action.iterator.aggregate.College;
import com.linguochao.design.action.iterator.aggregate.ComputerCollege;
import com.linguochao.design.action.iterator.aggregate.InfoCollege;
import com.linguochao.design.action.iterator.aggregate.University;

import java.util.ArrayList;
import java.util.List;

public class Client {

	public static void main(String[] args) {
		//创建学院
		List<College> collegeList = new ArrayList<College>();

		ComputerCollege computerCollege = new ComputerCollege();
		InfoCollege infoCollege = new InfoCollege();

		collegeList.add(computerCollege);
		collegeList.add(infoCollege);

		University university = new University(collegeList);
		university.printCollege();
	}

}
