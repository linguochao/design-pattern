package com.linguochao.design.action.visitor.element;

import com.linguochao.design.action.visitor.visitor.Action;

public abstract class Person {

	//提供一个方法，让访问者可以访问
	public abstract void accept(Action action);
}
