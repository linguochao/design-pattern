package com.linguochao.design.action.visitor;

import com.linguochao.design.action.visitor.element.Man;
import com.linguochao.design.action.visitor.element.Woman;
import com.linguochao.design.action.visitor.visitor.Fail;
import com.linguochao.design.action.visitor.visitor.Success;
import com.linguochao.design.action.visitor.visitor.Wait;

public class Client {

	public static void main(String[] args) {

		//创建ObjectStructure
		ObjectStructure objectStructure = new ObjectStructure();

		objectStructure.attach(new Man());
		objectStructure.attach(new Woman());


		Success success = new Success();
		objectStructure.display(success);

		System.out.println("===============");

		Fail fail = new Fail();
		objectStructure.display(fail);

		System.out.println("=======给的是待定的测评========");

		Wait wait = new Wait();
		objectStructure.display(wait);
	}

}
