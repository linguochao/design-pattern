package com.linguochao.design.action.visitor.element;

import com.linguochao.design.action.visitor.visitor.Action;

public class Man extends Person {

	@Override
	public void accept(Action action) {
		action.getManResult(this);
	}

}
