package com.linguochao.design.action.visitor.visitor;

import com.linguochao.design.action.visitor.element.Man;
import com.linguochao.design.action.visitor.element.Woman;

public abstract class Action {

	//得到男性 的测评
	public abstract void getManResult(Man man);

	//得到女的 测评
	public abstract void getWomanResult(Woman woman);
}
