package com.linguochao.design.action.command.tradition;

public interface Control {

	 void onButton(int slot);

	 void offButton(int slot);
	
	 void undoButton();
}
