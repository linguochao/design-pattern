# 忘录模式 -- 游戏角色状态恢复问题

### 需求:
游戏角色有攻击力和防御力，在大战Boss前保存自身的状态(攻击力和防御力)，当大战Boss后攻击力和防御力下降，从备忘录对象恢复到大战前的状态

**传统的方式的问题分析**
- 一个对象，就对应一个保存对象状态的对象，当我们游戏的对象很多时，不利于管理，开销也很大.

- 传统的方式是简单地做备份，new出另外一个对象出来，再把需要备份的数据放到这个新对象，但这就暴露了对象内部的细节

### 备忘录模式基本介绍
- 备忘录模式（MementoPattern）在不破坏封装性的前提下，捕获一个对象的内部状态，并在该对象之外保存这个状态。
  这样以后就可将该对象恢复到原先保存的状态

**原理类**
- originator:对象(需要保存状态的对象)

- Memento：备忘录对象,负责保存好记录，即Originator内部状态

- Caretaker:守护者对象,负责保存多个备忘录对象，使用集合管理，提高效率

**说明：**
如果希望保存多个originator对象的不同时间的状态，也可以，只需要要HashMap<String,集合>

### 备忘录模式的注意事项和细节
- 给用户提供了一种可以恢复状态的机制，可以使用户能够比较方便地回到某个历史的状态
- 实现了信息的封装，使得用户不需要关心状态的保存细节
- 如果类的成员变量过多，势必会占用比较大的资源，而且每一次保存都会消耗一定的内存,这个需要注意

**适用的应用场景：**
1、后悔药。
2、打游戏时的存档。
3、Windows里的ctri+z。
4、IE中的后退。
5、数据库的事务管理

