package com.linguochao.design.action.memento.game;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Memento {

	//攻击力
	private int vit;
	//防御力
	private int def;
	public Memento(int vit, int def) {
		super();
		this.vit = vit;
		this.def = def;
	}

}
