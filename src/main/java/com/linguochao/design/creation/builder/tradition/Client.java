package com.linguochao.design.creation.builder.tradition;

public class Client {

	public static void main(String[] args) {
		CommonHouse commonHouse = new CommonHouse();
		commonHouse.build();
	}

}
