package com.linguochao.design.creation.builder.improve;

public class Client {

	public static void main(String[] args) {

		//盖普通房子
		CommonBuilding commonBuilding = new CommonBuilding();
		//准备创建房子的指挥者
		HouseDirector houseDirector = new HouseDirector(commonBuilding);

		//完成盖房子，返回产品(普通房子)
		House commonHouse = houseDirector.constructHouse();

		System.out.println("输出流程"+commonHouse);

		System.out.println("--------------------------");

		//盖高楼
		HighBuilding highBuilding = new HighBuilding();
		//重置建造者
		houseDirector.setHouseBuilder(highBuilding);
		//完成盖房子，返回产品(高楼)
        House highHouse = houseDirector.constructHouse();

        System.out.println("输出流程"+highHouse);

    }
}
